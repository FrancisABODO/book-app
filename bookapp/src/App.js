/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable default-case */
import React, { useState, useEffect} from 'react';
import firebase from './Firebase.js';
import Login from './Components/Login';
import Book from './Components/Book.js';
import './App.css';

function App() {
  const [User, setUser] = useState('');
  const [Email, setEmail] = useState('');
  const [PassWord, setPassWord] = useState('');
  const [EmailError, setEmailError] = useState('');
  const [PassWordError, setPassWordError] = useState('');
  const [HasAccount, setHasAccount] = useState(false);

  // state for books
  

  const clearInputs = () => {
    setEmail('');
    setPassWord('');
  }

  const clearErrors = () => {
    setEmailError('');
    setPassWordError('');
  }

  const handleLogin = () => {
    clearErrors();
    firebase.auth().signInWithEmailAndPassword(Email, PassWord).catch((err) => {
      switch (err.code) {
        case "auth/invalid-email":
        case "auth/user-disabled":
        case "auth/user-not-found":
          setEmailError(err.message);
          break;
        case "auth/wrong-password":
          setPassWordError(err.message);
          break;
      }
    })
    console.log('user logged in');
  }
  const handleSignup = () => {
    clearErrors();
    firebase.auth().createUserWithEmailAndPassword(Email, PassWord).catch((err) => {
      switch (err.code) {
        case "auth/email-already-in-use":
        case "auth/invalid-email":
        case "auth/user-not-found":
          setEmailError(err.message);
          break;
        case "auth/weak-password":
          setPassWordError(err.message);
          break;
      }
    })
  }

  const handleLogout = () => {
    firebase.auth().signOut();
    console.log('user logged out');
  }

  const authListener = () => {
    firebase.auth().onAuthStateChanged((User) => {
      if(User) {
        clearInputs();
        setUser(User);
      } else {
        setUser('');
      }
    })
  }

  useEffect(() => {
    authListener();
  }, []);

  return (
    <div className="App">
      {User ? (
        <Book setHasAccount={setHasAccount} handleLogout={handleLogout}/>
      ) : (
        <Login 
          Email={Email}
          setEmail={setEmail}      
          PassWord={PassWord}
          setPassWord={setPassWord}
          handleLogin={handleLogin}
          handleSignup={handleSignup}
          HasAccount={HasAccount}
          setHasAccount={setHasAccount}
          EmailError={EmailError}
          PassWordError={PassWordError}
        />
      )}
    </div>
  );
}

export default App;
