/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react'
import { FormGroup, Label, Input, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'

import firebase from '../Firebase.js';


export default function Book({ handleLogout, setHasAccount }) {

    const [Books, setBooks] = useState([]);
    const db = firebase.firestore().collection("books");

    function getBooks(){
        setHasAccount(true);
        db.onSnapshot((snapshot) => {
            const items = [];
            snapshot.forEach((doc) => {
                items.push(doc.data());
            });
            setBooks(items);
        })
    }

    useEffect(() => {
        getBooks();
    }, [])

    let listBooks = Books.map((book) => {
        return (
            <tr key={book.id}>
                <td>{book.id}</td>
                <td>{book.titre}</td>
                <td>{book.image}</td>
                <td>{book.description}</td>
                <td>{book.rating}</td>
                <td style={{display: "flex"}}>
                    <Button color="success" size="sm" className="espace">Edit</Button>
                    <Button color="danger" size="sm">Delete</Button>
                </td>
            </tr>
        )
    })

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    /* const [NewBookData, setNewBookData] = useState([{ titre: '', description: '', rating: ''}])

    // ADD FUNCTION
    function addBook(newBook){
        db.add({
            titre: newBook.titre,
            description: newBook.description,
            rating: newBook.rating
        })
        toggle();
    }

    // DELETE FUNCTION
    function deleteBook(book){
        db.doc(book.id).delete().catch((err) => {
            console.error(err);
        });
    }

    // EDIT FUNCTION
    function editBook(updatedBook){
        setHasAccount();
        db.doc(updatedBook.id).update(updatedBook).catch((err) => {
            console.error(err);
        });
    } */

    return (
        <section className="book">
            <nav>
                <h2>Welcome to Our Library</h2>
                <button onClick={handleLogout}>Logout</button>
            </nav>     
            <Button color="primary" onClick={toggle} className="addBook">Add Book</Button>
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Add a new Book</ModalHeader>
                <ModalBody>
                    <FormGroup className="mt-1">
                        <Label for="titre">Titre</Label>
                        <Input id="title" />{/* value={NewBookData.titre} onChange={(e) => {
                            //let { NewBookData } = NewBookData;
                            NewBookData.titre = e.target.value;
                            setNewBookData({ NewBookData });
                        }} />             */}
                    </FormGroup>                    
                    <FormGroup className="mt-3">
                        <Label for="description">Description</Label>
                        <Input id="description" />{/* value={NewBookData.description} onChange={(e) => {
                            NewBookData.description = e.target.value;
                            setNewBookData({ NewBookData });
                        }} />             */}
                    </FormGroup>                    
                    <FormGroup className="mt-3">
                        <Label for="rating">Rating</Label>
                        <Input id="rating" />{/* value={NewBookData.rating} onChange={(e) => {
                            NewBookData.rating = e.target.value;
                            setNewBookData({ NewBookData });
                        }} />             */}
                    </FormGroup>  
                    <FormGroup className="mt-3">
                        <Label for="image">Image</Label>
                        <Input type="file" id="file" />
                        {/* <FormText color="muted">
                        This is some placeholder block-level help text for the above input.
                        It's a bit lighter and easily wraps to a new line.
                        </FormText> */}
                    </FormGroup>                  
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={toggle}>Add Book</Button>{' '}
                    <Button color="secondary" onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
            <div className="bookList container">


                <Table bordered>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Image</th>
                            <th>Description</th>
                            <th>Rating</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {listBooks}
                    </tbody>
                </Table>
            </div>       
        </section>
    )
}
