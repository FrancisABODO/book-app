import React from 'react'

export default function Login(props) {

    const { Email, setEmail, PassWord, setPassWord, handleLogin, handleSignup, HasAccount, setHasAccount, EmailError, PassWordError } =  props;

    return (
        <section className="login">
            <div className="loginContainer">
                <label>Username</label>
                <input type="text" autoFocus required value={Email} onChange={(e) => setEmail(e.target.value)} />
                <p className="errorMsg">{EmailError}</p>
                <label>Password</label>
                <input type="password" required value={PassWord} onChange={(e) => setPassWord(e.target.value)} />
                <p className="errorMsg">{PassWordError}</p>
                <div className="btnContainer">
                    {HasAccount ? (
                        <>
                            <button onClick={handleLogin}>Sign in</button>
                            <p>
                                Don't have an account ? 
                                <span onClick={(e) => setHasAccount(!HasAccount)}>Sign up</span>
                            </p>
                        </>
                    ) : (
                        <>
                            <button onClick={handleSignup}>Sign up</button>
                            <p>
                                Have an account ? 
                                <span onClick={(e) => setHasAccount(!HasAccount)}>Sign in</span>
                            </p>
                        </>
                    )}
                </div>
            </div>
        </section>
    )
}
