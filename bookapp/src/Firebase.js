import firebase from "firebase";
import 'firebase/firestore';

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyDgU6TquAMPBraXXsHipfF_Dp5EI6MHJOs",
    authDomain: "bookapp-9f28b.firebaseapp.com",
    projectId: "bookapp-9f28b",
    storageBucket: "bookapp-9f28b.appspot.com",
    messagingSenderId: "816544127754",
    appId: "1:816544127754:web:decef00164e6ec081538f3",
    measurementId: "G-S6D7L5G2QL"
  };

  firebase.initializeApp(firebaseConfig);

  export default firebase;